class_name Place
extends Node2D


var place_name   : String # complete name shown in menus
var place_handle : String # short name used in code

@export var bg_images : Array[Texture2D] # images to show on top of each other to form the scene
@export var bg_layers : Array[int]       # z order of each bg image. additional elements will have no effect and missing ones will be set to 0
