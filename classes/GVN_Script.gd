# the Godot Visual Novel scripting aims to reproduce VN scripting methods
# such as renpy in its syntax and ease of use

class_name GVN_Script


# script
var path : String
var file : FileAccess
var pos  : int = 0

# pre pass data
var LABELS     : Dictionary = {} # LABEL  -> label_pos
var CONDITIONS : Dictionary = {} # if_pos -> {"ELSE" -> else_pos, "ENDIF" -> endif_pos}

# are we currently playing the line of dialogue of a character?
var SPEAKER : String
var TIRADE  : bool = false
var TRANS   : bool = false


func _init(script_path:String):
	
	self.path = script_path
	self.file = FileAccess.open(script_path, FileAccess.READ)
	
	if FileAccess.get_open_error() != OK:
		printerr("Failed to load GVN script file '%s'" % script_path)
	
	pre_pass()


# this will store LABEL positions for efficient jumps and calls
# IMPORTANT NOTE: when getting a new line with FileAccess, the cursor jumps to the beginning of the new line!
func pre_pass():
	
	var line       : String = ""
	var label_name : String = ""
	var tmp_ifs    : Array  = []
	
	file.seek(0)
	
	while not file.eof_reached():
		
		line     = file.get_line().strip_edges()
		self.pos = file.get_position()
		
		# the line empty or a comment
		if line.is_empty() or line.begins_with("#") or line.begins_with("//"):
			continue
		
		var keyword = line.get_slice(" ", 0).to_lower()
		
		match keyword:
			
			# the line is a LABEL declaration
			
			"label":
				label_name = line.get_slice(" ", 1)
				self.LABELS[label_name] = self.pos
			
			# conditional statements
			
			# create an entry for the new IF statement we found
			"if":
				tmp_ifs.push_front(self.pos)
				self.CONDITIONS[tmp_ifs[0]] = {}
			
			# add the position of the ELSE statement of the latest IF
			"else":
				self.CONDITIONS[tmp_ifs[0]]["else"] = self.pos
			
			# we ended an IF, so we add the position of the ENDIF to the latest
			# IF, and go back down by one to close it
			"endif":
				self.CONDITIONS[tmp_ifs[0]]["endif"] = self.pos
				tmp_ifs.pop_front()
	
	#for i in self.CONDITIONS.keys():
	#	print(i, ": ", self.CONDITIONS[i])
