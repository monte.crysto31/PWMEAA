extends Node


@onready var umi_breakup : Resource = preload("res://effects/UmiBreakup/UmiBreakup.tscn")
@onready var court_slide : Resource = preload("res://effects/CourtSlide/CourtSlide.tscn")


# char1 and char2 are the starting and ending characters we slide from/to
# move is one of ["lr", "rl", "lm", "ml", "rm", "mr"] and indicates the direction
# place is one of ["", "eng_"] and indicates if the backgrounds/foregrounds are from LA or the UK
func CourtSlide(char1:String, char2:String, move:String, place:String = ""):
	
	var x1  : float
	var x2  : float
	var dur : float = 0.5
	var new_scene : String = ""
	
	match move:
		
		"lr":
			x1 =    0.
			x2 = 7680.
			new_scene = "court_desk_prosecution" + place
		"rl":
			x1 = 7680.
			x2 =    0.
			new_scene = "court_desk_defense" + place
		"lm":
			x1 =    0.
			x2 = 3840.
			new_scene = "court_desk_witness" + place
		"ml":
			x1 = 3840.
			x2 =    0.
			new_scene = "court_desk_defense" + place
		"rm":
			x1 = 7680.
			x2 = 3840.
			new_scene = "court_desk_witness" + place
		"mr":
			x1 = 3840.
			x2 = 7680.
			new_scene = "court_desk_prosecution" + place
	
	# prep the next scene underneath the transition
	
	SignalBus.scene.emit(new_scene)
	SignalBus.show .emit(char2, "origin", "")
	
	# transition
	
	var court_slide_inst : Node2D = court_slide             .instantiate()
	var char1_inst       : Node2D = Scenes.Characters[char1].instantiate()
	var char2_inst       : Node2D = Scenes.Characters[char2].instantiate()
	
	char1_inst.position.x = x1 * 0.5
	char2_inst.position.x = x2 * 0.5
	
	court_slide_inst.get_node("Chars").add_child(char1_inst)
	court_slide_inst.get_node("Chars").add_child(char2_inst)
	
	get_tree().root.add_child(court_slide_inst)
	
	SignalBus.camera_span.emit(x1, x2, dur)
	SignalBus.transition_started.emit()
	
	await SignalBus.transition_finished
	
	court_slide_inst.hide()
	court_slide_inst.queue_free()


func UmiBreakup(direction:String):
	
	var umi_breakup_inst : Node2D = umi_breakup.instantiate()
	umi_breakup_inst.base_texture = \
		ImageTexture.create_from_image(get_viewport().get_texture().get_image())
	
	get_tree().root.add_child(umi_breakup_inst)
	umi_breakup_inst.burst(int(direction))
	
	SignalBus.transition_started.emit()
	
	await SignalBus.transition_finished
