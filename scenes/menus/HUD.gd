extends CanvasLayer


func hide_dialogue_box():
	$DialogueBox.hide()

func show_dialogue_box():
	$DialogueBox.show()


func hide_quick_menu():
	$QuickMenu.hide()

func show_quick_menu():
	$QuickMenu.show()
