extends Control


func _ready() -> void:
	
	$MainButtons/Buttons/LoadGame  .pressed.connect(show_menu.bind("Load"))
	$MainButtons/Buttons/Options   .pressed.connect(show_menu.bind("Options"))
	$MainButtons/Buttons/PictureBox.pressed.connect(show_menu.bind("Pictures"))
	$MainButtons/Buttons/MusicBox  .pressed.connect(show_menu.bind("Music"))
	$MainButtons/Buttons/Portfolio .pressed.connect(show_menu.bind("Portfolio"))
	$MainButtons/Buttons/Disclaimer.pressed.connect(show_menu.bind("Disclaimer"))


func hide_menus():
	
	$BG/BG1.show()
	$BG/BG2.hide()
	
	for menu in $SubMenus.get_children():
		menu.hide()
		menu.reset()
	
	$SubMenus.hide()
	$MainButtons.show()


func show_menu(menu:String):
	
	$BG/BG1.hide()
	$BG/BG2.show()
	
	$MainButtons.hide()
	$SubMenus.show()
	
	match menu:
		
		"Load":
			$SubMenus/Load.show()
		
		"Options":
			$SubMenus/Options.show()
		
		"Pictures":
			$SubMenus/PictureBox.show()
		
		"Music":
			$SubMenus/MusicBox.show()
		
		"Portfolio":
			$SubMenus/Portfolio.show()
		
		"Disclaimer":
			$SubMenus/Disclaimer.show()
